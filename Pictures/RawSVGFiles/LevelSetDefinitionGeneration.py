# Computing the distance and location w.r.t. the subdomain through the shapely information 				
from shapely.geometry import MultiLineString as ShapelyMultiLineString
from shapely.geometry import MultiPolygon    as ShapelyMultiPolygon
from shapely.geometry import Polygon         as ShapelyPolygon
from shapely.geometry import Point           as ShapelyPoint
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import matplotlib.pyplot as plt

import numpy as np
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
fig = plt.figure()
ax = plt.axes(projection='3d')
ax.view_init(elev=-115., azim=18)
res = 20

f = lambda x,y:  np.exp((-(x+0.2)**(2)-(y+0.2)**(2)))*(10*(x+0.2)**(3)-2*(x+0.2)+10*(y+0.2)**(5))
g = lambda x,yy: ((xx-0.3)/2)**4+((yy-0.5)/2)**4-1

x = np.linspace(-1.8,2.4,res)
y = np.linspace(-1.6,2.6,res)
xx, yy = np.meshgrid(x,y)
xl = xx.ravel()
yl = yy.ravel()

cs1 = plt.contour(xx, yy, g(xx,yy),levels=[0], linewidths=1.2)
cs  = plt.contour(xx, yy, f(xx,yy)-2,levels=[0], colors=[(0.21,0.72,0.65,1)], linewidths=5)
xy1 = cs1.allsegs[0][0]
xy  = cs.allsegs[0][0]

Borders =  [tuple(tuple(a) for a in xy)]
FluidBorder = ShapelyMultiLineString(Borders)
Area = ShapelyPolygon([(a[0], a[1]) for a in xy])

# Computing the distance and location w.r.t. the subdomain through the shapely information
Value = np.array([[ShapelyPoint(tuple(pt)).distance(FluidBorder),\
                    int(2*Area.intersects(ShapelyPoint(tuple(pt)).buffer(1e-14))-1)] \
                    for pt in np.array([xl,yl]).T]).T
val = 4*np.prod(Value, axis=0)
#val= np.reshape(val,(res,res))



verts = [list(zip(xy1[:,0], xy1[:,1], xy1[:,0]*0))]
ax.add_collection3d(Poly3DCollection(verts, facecolors=[(0.75,0.98,0.71)], alpha=0.2))
verts = [list(zip(xy[:,0], xy[:,1], xy[:,0]*0))]
ax.add_collection3d(Poly3DCollection(verts, facecolors=[(0.61,0.78,1)], alpha=0.6))
ax.plot_trisurf(xl, yl, val,color=(0.61,0.7,1), alpha=0.6, edgecolor='none')
cs  = plt.contour(xx, yy, f(xx,yy)-2,levels=[0], colors=[(0.21,0.72,0.65,1)], linewidths=5, alpha=1)

ax.grid(False)


#ax.text(x, y, z, label, zdir)
# Hide axes ticks
ax.set_xticks([])
ax.set_yticks([])
ax.set_zticks([])
plt.axis("off")
ax.view_init(elev=18., azim=-115)
plt.savefig('LevelSetInitialisation.png', dpi=720)
plt.show()
