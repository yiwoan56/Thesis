# Make the folder name the file name of the concatenated file
# Define new command of the name of each single file and paste the content of the respective single file in it
# Copy the header at the top of the file
import os

# Get the files to create
Elements = [f for f in os.scandir(".") if f.is_dir()]

# For each file to create, sweep on the number of figures to create
for ele in Elements:
    
    # Initialising the content of the full pictures file
    RunningContent = ""
    
    # Getting the header common to all the pictures
    Header = open(os.path.join(ele,"_GraphicsHeader.txt"), "r")
    HeaderContent = Header.read()
    
    # Concatenating all the picture
    PictureNames = [f for f in os.scandir(ele) if ".tikz" in f.name]
    for Pic in PictureNames:
        TikzFile    = open(Pic, "r")
        TikzContent = TikzFile.read()
        TikzFile.close()
        
        PicContent = "\\newcommand{\\"+Pic.name[:-5]+"}{\n"\
                     + TikzContent\
                     + "}\n\n"
                 
        RunningContent += PicContent
    
    # Filling the new concatenated file
    FileContent = HeaderContent + RunningContent
    OutFile = open(os.path.join("../",ele.name+".tex"), "w")
    OutFile.write(FileContent)
    OutFile.close()
    
    
