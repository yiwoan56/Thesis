﻿\begin{tikzpicture}[line cap=round,line join=round,x=1cm,y=1cm, xscale=1,>=triangle 45,]
    
    % Crop
	\clip(-6.5,-1.99) rectangle (0.5,7.08);
    
    % Geometrical entities
	\fill[style=PrimalElement, xscale=1.7] (-2.03,2.66) -- (0.16,2.52)   -- (-0.95,0.18) -- cycle;
	\fill[style=PrimalElement, xscale=1.7] (-3.3,4.05) -- (-2.09,5.80)  -- (-2.03,2.66) -- cycle;
	\fill[style=PrimalElement, xscale=1.7] (-2.09,5.80) -- (-2.03,2.66)  -- (-0.16,6.01) -- cycle;
	\fill[style=PrimalElement, xscale=1.7,fill opacity=0.4] (-2.03,2.66) -- (-0.16,6.01) -- (0.16,2.52) -- cycle;
      
	\fill[style=ControlVolume, xscale=1.7,fill opacity=0.8] (-2.03,2.66) -- (-0.16,6.01) -- (0.16,2.52) -- cycle;
    \fill [style=VoFVolume,xscale=1.7,opacity=0.4]    (-0.16,6.01) -- (0.16,2.52)   -- (-0.95,2.60) .. controls (-1,2.97) and (-1,2.65) .. (-1.1,4.36) -- (-0.16,6.01)  ;
    
	\draw [style=PrimalEdge, xscale=1.7] (0.16,2.52)   -- (-0.95,0.18);
	\draw [style=PrimalEdge, xscale=1.7] (-0.95,0.18)  -- (-2.03,2.66);
	\draw [style=PrimalEdge, xscale=1.7] (-0.16,6.01)  -- (0.16,2.52);
	\draw [style=PrimalEdge, xscale=1.7] (0.16,2.52)   -- (-2.03,2.66);
	\draw [style=PrimalEdge, xscale=1.7] (-2.03,2.66)  -- (-0.16,6.01);
	\draw [style=PrimalEdge, xscale=1.7] (-0.16,6.01)  -- (-2.09,5.80);
	\draw [style=PrimalEdge, xscale=1.7] (-3.3,4.05)  -- (-2.09,5.80);
	\draw [style=PrimalEdge, xscale=1.7] (-2.09,5.80)  -- (-2.03,2.66);
	\draw [style=PrimalEdge, xscale=1.7] (-2.03,2.66)  -- (-3.3,4.05);
	\draw[style=ControlVolume,xscale=1.7,draw opacity=1, line width=1.2, fill opacity=0] (-2.03,2.66) -- (-0.16,6.01) -- (0.16,2.52) -- cycle;
    \draw [style=Interface, xscale=1.7, xshift=18.8, yshift=15] (-1.8, -1.98) .. controls (0., 4.5) and (-2.5,-2) .. (-1.5, 7.08);
	
	% Labels
	\draw [style=VoFText, opacity=1] (-1.2,4.9)   node[anchor=north west] {$\bHi$};
	\draw [style=PrimalText]    (-3.2,5.1) node[anchor=north west] {$K^-$};
	\draw [style=PrimalText]    (-2.95,3.3) node[anchor=north west] {\scriptsize $\phi^K(x) \quad \forall x\in K$};
	\draw [style=ControlVolume, opacity=1]    (-2.2,3.9) node[anchor=north west] {$\Cv_i=K$};
	\draw [style=PrimalText]    (-6.1,0.19) node[anchor=north west] {\footnotesize Level set reconstruction};
	\draw [style=PrimalText]    (-6.1,-0.2) node[anchor=north west] {\footnotesize Degrees of freedom of $\phi^K$};
	\draw [style=ControlVolume]    (-6.1,0.6) node[anchor=north west] {\footnotesize Control volume};

	\fill [style=PrimalNode,xshift=-3] (-6.2,-0.47) circle(3pt);
	%\draw [style=ControlVolume, fill opacity=0,->, line width=1] (-6.1,0.3) .. controls (-6.8,0.2) and (-4,5.5) ..  (-2.1,3.7);
	\draw [style=ControlVolume, line width=1, xscale=1.7,yshift=0.1]    (-3.8,0.2)  rectangle (-3.6,0.4);	

	\draw [style=PrimalNode,->, line width=1] (-1.9,-0.1) .. controls (-1.5,0.2) and (-5,1.3) ..  (-2.9,2.9);
    \fill [style=PrimalNode, xscale=1.7]    (-2.03,2.66) circle (3pt);
	\fill [style=PrimalNode, xscale=1.7]    (-0.16,6.01) circle (3.pt);
	\fill [style=PrimalNode, xscale=1.7]    (0.16,2.52)  circle (3.pt);	
	
\end{tikzpicture}
