﻿\begin{tikzpicture}[line cap=round,line join=round,x=1cm,y=1cm]
  
    % Crop
	\clip(-6.5,0.0) rectangle (0.5,7.08);
    
    % Geometrical entities
	\fill[style=PrimalElement, xscale=1.7] (-2.03,2.66) -- (0.16,2.52)   -- (-0.95,0.18) -- cycle;
	\fill[style=PrimalElement, xscale=1.7] (-3.3,4.05) -- (-2.09,5.80)  -- (-2.03,2.66) -- cycle;
	\fill[style=PrimalElement, xscale=1.7] (-2.09,5.80) -- (-2.03,2.66)  -- (-0.16,6.01) -- cycle;
	\fill[style=PrimalElement, xscale=1.7] (-2.03,2.66) -- (-0.16,6.01) -- (0.16,2.52) -- cycle;
  	
    \draw [style=InterfacialEdge, xscale=1.7,line width=1.5] (0.16,2.52)   -- (-2.03,2.66);
    \draw [style=InterfacialEdge, xscale=1.7,line width=1.5] (-2.03,2.66)  -- (-0.16,6.01);

	\fill[style=VoFVolume,opacity=0.4,line width=1.8pt]  (-0.8,1.2) .. controls (-2.8,1.8) and (-1.6,4.2) .. (-1.6, 4.64) -- (-3.44,2.66) -- (-1.6,0.18)  ;
	\fill[style=VoFVolume,opacity=0.4,line width=1.8pt]  (-0.8,1.2) (-1.6, 4.64) .. controls (-1.2,5.5) and (-1.5,5.8) .. (-2.7, 5.85)--(-3.53,5.80)--(-3.44,2.65);

	\draw [style=PrimalEdge, xscale=1.7] (0.16,2.52)   -- (-0.95,0.18);
	\draw [style=PrimalEdge, xscale=1.7] (-0.95,0.18)  -- (-2.03,2.66);
	\draw [style=PrimalEdge, xscale=1.7] (-0.16,6.01)  -- (0.16,2.52);
	\draw [style=PrimalEdge, xscale=1.7] (-0.16,6.01)  -- (-2.09,5.80);
	\draw [style=PrimalEdge, xscale=1.7] (-3.3,4.05)  -- (-2.09,5.80);
	\draw [style=PrimalEdge, xscale=1.7] (-2.09,5.80)  -- (-2.03,2.66);
	\draw [style=PrimalEdge, xscale=1.7] (-2.03,2.66)  -- (-3.3,4.05);
	
	\draw [style=Interface,line width=2.3,dashed, dash pattern=on 4pt off 6pt] (-0.8,1.2) .. controls (-2.8,1.8) and (-1.6,4.2) .. (-1.6, 4.64);
	\draw [style=Interface,line width=2.3,dashed, dash pattern=on 4pt off 6pt] (-1.6, 4.64) .. controls (-1.2,5.5) and (-1.5,5.8) .. (-2.7, 5.85) ;
	\draw [style=Interface,line width=2.3]  (-1.97, 2.58) .. controls (-1.1,3.2) and (-1.65,3) .. (-1.6, 4.64);
	\draw [style=Interface,line width=2.3]  (-0.82,1.19) .. controls (-3.4,1.8) and (-2.1,2.5) ..  (-1.97, 2.58) ;
	\draw [style=Interface,line width=2.3] (-1.6, 4.64) .. controls (-1.65,5.8) and (-2.7,5.9) .. (-2.7, 5.84) ;

	\draw [style=InterfacialEdge] (-1.6, 4.63) node[mark size=2pt] {\pgfuseplotmark{square*}};
	\draw [style=InterfacialEdge] (-2.7, 5.85)  node[mark size=2pt] {\pgfuseplotmark{square*}};
	
	% Labels and legend
	\draw [style=VoFText] (-3,5.2)   node[anchor=north west] {$\bar{H}_i$};
	\draw [style=VoFText] (-3,3.3)   node[anchor=north west] {$\bar{H}_i$};
	\draw [style=VoFText] (-1.95,1.2)   node[anchor=north west] {$\bar{H}_i$};
	
	\begin{scope}[yshift=5em, xshift=-9.5em]
	\begin{scriptsize}
		\draw [style=Interface,line width=1.3,xshift=-2.8em]        (-0.9, -0.8) --  (-1.1, -0.8);
		\draw [style=Interface,line width=1.3,xshift=-2.8em,dashed, dash pattern=on 2pt off 2pt] (-0.9, -1.15) -- (-1.1, -1.15);
		\draw [xshift=-1.2em]  (-1.45, -1.46) node[InterfacialEdge, mark size=2pt] {\pgfuseplotmark{square*}};

		\node [color=InterfaceColor, align=left, anchor=mid west, xshift=-3em]  at (-0.8, -1.15) {Corrected interface};
		\node [color=InterfaceColor, align=left, anchor=mid west, xshift=-3em]  at (-0.8, -0.8) {Initial interface};
		\node [color=InterfacialEdgesColor, align=left, anchor=mid west, xshift=-3em]  at (-0.8, -1.5) {Preserved interface point};
	\end{scriptsize}
    \end{scope}
	
\end{tikzpicture}
