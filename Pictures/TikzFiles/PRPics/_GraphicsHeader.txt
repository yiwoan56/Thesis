% Tikz dependencies
\usepgfplotslibrary{fillbetween}
\usetikzlibrary{decorations.pathreplacing}
\usetikzlibrary{shapes.misc}
\usetikzlibrary{arrows.meta}
\usetikzlibrary{backgrounds}
\usetikzlibrary{positioning}
\usetikzlibrary{plotmarks}
\usetikzlibrary{patterns}
\usetikzlibrary{arrows}
\usetikzlibrary{matrix}
\usetikzlibrary{shapes}
\usetikzlibrary{calc}
\usetikzlibrary{fit}

% Importing the colorscheme
\input{Pictures/FiguresColorScheme}

% Tweaks
\makeatletter
\tikzset{
  double arrow/.style args={#1 colored by #2 and #3}{
    -stealth,line width=#1,#2, % first arrow
    postaction={draw,-stealth,#3,line width=(#1)/3,
                shorten <=(#1)/3,shorten >=2*(#1)/3}, % second arrow
  }
}
\makeatother

\makeatletter 
\tikzset{
  reuse path/.code={\pgfsyssoftpath@setcurrentpath{#1}},
  even odd clip/.code={\pgfseteorule}}
\makeatother


\makeatletter
\pgfutil@ifundefined{pgf@pattern@name@_ho9kqrk36}{
	\pgfdeclarepatternformonly[\mcThickness,\mcSize]{_ho9kqrk36}
	{\pgfqpoint{0pt}{-\mcThickness}}
	{\pgfpoint{\mcSize}{\mcSize}}
	{\pgfpoint{\mcSize}{\mcSize}}
	{
		\pgfsetcolor{\tikz@pattern@color}
		\pgfsetlinewidth{\mcThickness}
		\pgfpathmoveto{\pgfqpoint{0pt}{\mcSize}}
		\pgfpathlineto{\pgfpoint{\mcSize+\mcThickness}{-\mcThickness}}
		\pgfusepath{stroke}
}}
\makeatother
