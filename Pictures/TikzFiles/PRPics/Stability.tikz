﻿\begin{tikzpicture}[x=0.75pt,y=0.75pt,yscale=-1,xscale=1,>={Latex[length=1.2mm, width=1.2mm]}]
    \clip (0,210) rectangle +(500,-125);

    % Initial data
    \begin{scope}[xshift=-25]
        \draw [<->, xshift=-78.5] (188,106)--(188,175)--(275,175) ;
        \draw (118,186) node [scale=0.7] [anchor=north, align=center] {\textit{Initial data}};
        \draw    (157.55,163.52) -- (118.55,163.52) ;
        \draw    (118.55,163.52) -- (118.46,123.57) ;
        \draw    (118.46,123.57) -- (86.32,122.77) ;
        \draw  [color=BaseColor,draw opacity=1, line width=0.75] (87,121.25) .. controls (92.24,121.25) and (88.3,123.93) .. (90,124.21) .. controls (95.84,125.17) and (92.8,120.66) .. (95,121.74) .. controls (95.59,122.03) and (96.65,124.87) .. (98,124.21) .. controls (99.08,123.68) and (98.21,121.25) .. (100,121.25) .. controls (102.03,121.25) and (99.44,123.57) .. (101,124.21) .. controls (109.24,127.59) and (106.24,122.03) .. (108,121.74) .. controls (113.11,120.9) and (111.32,122.87) .. (112,124.21) .. controls (112.36,124.92) and (112.95,122.76) .. (114,122.24) .. controls (117.03,120.74) and (118,123.15) .. (118,123.71) ;
        \draw  [color=BaseColor,draw opacity=1, line width=0.75]  (119,162.78) .. controls (124.89,168.68) and (126.43,160.5) .. (129,161.78) .. controls (132.69,163.63) and (137.14,166.23) .. (141,164.78) .. controls (141.94,164.43) and (141,162.78) .. (141,161.78) .. controls (141,160.58) and (142,164.12) .. (143,164.78) .. controls (146.6,167.18) and (146.12,162.41) .. (148,161.78) .. controls (150.27,161.03) and (158,169.46) .. (158,162.78) ;
    \end{scope}

    % Evolved stable	
    \begin{scope}
        \draw  [dash pattern={on 0.84pt off 2.51pt}]  (192.55,116.52) .. controls (267.55,111.52) and (222.55,158.52) .. (284.55,154.52) ;
        \draw  [dash pattern={on 0.84pt off 2.51pt},yshift=1.5]  (192.55,129.52) .. controls (261.55,124.52) and (216.55,171.52) .. (284.55,167.52) ;
        \draw    (192.55,123.52) .. controls (263.55,118.52) and (218.55,165.52) .. (280.55,161.52) ;
        \draw (236,186) node [scale=0.7] [anchor=north, align=center] { \textit{Stable behaviour}};
        \draw [<->] (188,106)--(188,175)--(287,175) ;
        \draw  [color=BaseColor,draw opacity=1,line width=0.75]  (198,125.78) .. controls (199.57,124.22) and (203.16,120.36) .. (206,121.78) .. controls (207.13,122.35) and (209.25,127.63) .. (210,127.78) .. controls (214.36,128.66) and (223.72,120.5) .. (227,123.78) .. controls (229.31,126.1) and (224.42,131.41) .. (229,132.78) .. controls (231.94,133.67) and (237.4,131.77) .. (238,134.78) .. controls (238.57,137.62) and (237.94,141.12) .. (239,143.78) .. controls (240.58,147.74) and (247.32,144.73) .. (248,148.78) .. controls (248.66,152.74) and (246.16,157.95) .. (249,160.78) .. controls (250.26,162.04) and (254.88,162.01) .. (256,161.78) .. controls (256.16,161.75) and (259.08,157.86) .. (260,158.78) .. controls (261.53,160.32) and (264.3,163.02) .. (268,161.78) .. controls (269.17,161.39) and (273.15,157.4) .. (275,158.78) .. controls (276.55,159.95) and (278.06,161.78) .. (280,161.78) ;
    \end{scope}

    % Evolved unstable
    \begin{scope}[xshift=10em, yshift=-7em]
        \draw [<->, xshift=3, yshift=7em] (188,106)--(188,175)--(275,175) ;
        \draw (362,186)[xshift=-8em,yshift=7em] node [scale=0.7] [anchor=north, align=center] { \textit{Unstable behaviour}};
        \draw  (198.55,231.52) .. controls (269.55,226.52) and (224.55,273.52) .. (286.55,269.52) ;
        \draw  [dash pattern={on 0.84pt off 2.51pt}]  (198.55,237.52) .. controls (267.55,232.52) and (222.55,279.52) .. (286.55,275.52) ;
        \draw  [dash pattern={on 0.84pt off 2.51pt}]  (198.55,224.52) .. controls (273.55,219.52) and (228.55,266.52) .. (286.55,262.52) ;
        \draw  [color=BaseColor ,draw opacity=1,line width=0.75]  (197.65,218.55) .. controls (206.97,227.29) and (201.11,248.44) .. (201.36,257.73) .. controls (201.43,260.06) and (202.73,263.83) .. (206.01,262.96) .. controls (222.05,258.66) and (214.37,231.18) .. (215.3,222.9) .. controls (215.48,221.27) and (217.85,220.65) .. (219.02,219.42) .. controls (220.5,217.86) and (220.5,214.2) .. (222.73,214.2) .. controls (223.45,214.2) and (226.42,219.85) .. (226.45,220.29) .. controls (227.16,230.88) and (226.94,241.06) .. (229.24,250.77) .. controls (229.75,252.92) and (231.28,254.76) .. (232.03,256.86) .. controls (232.12,257.12) and (233.27,258.31) .. (233.88,257.73) .. controls (238.62,253.3) and (236.85,244.21) .. (237.6,238.58) .. controls (238.16,234.4) and (243.5,224.47) .. (245.04,219.42) .. controls (246.18,215.67) and (246.93,211.43) .. (247.82,208.1) .. controls (248.3,206.32) and (248.95,204.58) .. (249.68,202.88) .. controls (249.91,202.34) and (249.68,200.56) .. (249.68,201.14) .. controls (249.68,222.1) and (250.39,281.28) .. (250.61,285.59) .. controls (250.76,288.45) and (251.43,294.33) .. (254.33,293.43) .. controls (261.2,291.28) and (260.68,283.34) .. (262.69,278.63) .. controls (263.18,277.48) and (265.37,277.24) .. (265.48,276.02) .. controls (265.64,274.2) and (265.06,261.29) .. (270.12,260.34) .. controls (270.88,260.2) and (272.85,263.2) .. (272.91,263.83) .. controls (273.43,269.16) and (271.58,276.64) .. (277.56,277.76) .. controls (291.57,280.38) and (287.58,261.21) .. (293.35,261.21) ;
    \end{scope}
    
\end{tikzpicture}
