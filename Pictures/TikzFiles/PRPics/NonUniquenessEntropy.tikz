﻿\begin{tikzpicture}[y=2.5pt, x=2.5pt, yscale=-1, xscale=1.3, inner sep=0pt, outer sep=0pt,>={Latex[length=3mm, width=3mm]}]
    
    % Local color scheme
    \definecolor{TransitionLayer}{RGB}{24,88,144}
    \definecolor{IntermediateValue}{RGB}{188,0,0}
    \definecolor{FrontSpeed}{RGB}{0,151,0}
    \definecolor{cbe0000}{RGB}{190,0,0}
    \definecolor{FrontType}{RGB}{77,144,212}

    \definecolor{PrimarySol}{rgb}{0,0,0}
    \definecolor{SecondarySol}{rgb}{0.6,0.6,0.6}

   
    % Transition layers
    \fill[color=TransitionLayer,fill opacity=0.1] (45.8881,179.6112) rectangle (52.7377,255.8985);
    \fill[color=TransitionLayer,fill opacity=0.1] (101.6694,179.4956) rectangle (106.8871,255.7830);
    \fill[color=TransitionLayer,fill opacity=0.1]  (121.9172,179.3067) rectangle (136.1872,255.5940);
    \fill[color=TransitionLayer,fill opacity=0.1]  (72.3411,179.6356) rectangle (77.0628,255.9229);

    % Solution lines 
    \draw[color=black,line width=2] (46.0627,183.4685) -- (36.3728,183.4685);					% Sector 1
    \draw[color=PrimarySol,line width=2] (46.0627,183.4685).. controls (51.9201,183.4685) and (45.6733,196.3255) .. (52.7984,195.8785);% Sector 2
    \draw[color=SecondarySol,dash pattern=on 8pt off 2pt,dash pattern=on 8pt off 2pt,line width=2]  (52.67,220.4941) -- (47.5478,220.4891) -- (47.5102,183.4685) -- (46.0627,183.4685);% Sector 2
    \draw[color=SecondarySol,dash pattern=on 8pt off 2pt,line width=2] (52.7984,195.8785) -- (72.3643,195.8444);		% Sector 3
    \draw[color=PrimarySol,line width=2] (52.6550,220.4941) -- (72.37,220.4941);					% Sector 3  
    \draw[color=PrimarySol,line width=2]  (72.3643,195.8444) -- (73.5576,195.8444) -- (73.5576,230.6976)-- (77.0617,230.6976);					% Sector 4
    \draw[color=SecondarySol,dash pattern=on 8pt off 2pt,line width=2]  (72.37,220.4941).. controls (77.4378,221) and (73.9432,206.9) .. (77.0789,207.1492) ;	% Sector 4
    \draw[color=PrimarySol,line width=2] (77.0789,207.1492) -- (105.4480,207.1492) -- (105.4480,194.3768) -- (122.1735,194.3768) ;					% Sector 5-6-7
    \draw[color=SecondarySol,dash pattern=on 8pt off 2pt,line width=2, yshift=58.8] (77.0789,207.1492) -- (105.4480,207.1492) -- (105.4480,194.3768) -- (122.1735,194.3768) ;					% Sector 5-6-7
    \draw[color=PrimarySol,line width=2] (121.9374,217.9) .. controls (133.5728,218.1950) and (120.1346,248.3107) .. (136.1966,248.2107);	% Sector 8
    \draw[color=SecondarySol,dash pattern=on 8pt off 2pt,line width=2] (122.1735,194.3768)  -- (133.8780,194.3768) -- (133.8780,248.2107) -- (138.4701,248.2107);			% Sector 8
    \draw[color=black,line width=2] (136.1966,248.2107) -- (142.7784,248.2107);				% Sector 9

    % Unknwon nodes
    \node (A1) at  (90.8,218) [anchor=west]   {$\checkmark$};
    \node (A1) at  (103,215.2) [anchor=west, color=FrontSpeed]   {\textbf{?}};
    \node (A1) at  (61,184) [anchor=west, color=IntermediateValue]  {\textbf{?}};	  
    \node (A1) at  (88.5,184) [anchor=west, color=IntermediateValue]  {\textbf{?}};
    \node (A1) at  (127.5,251) [anchor=west, color=FrontType] {\textbf{?}};
    \node (A1) at (48,251) [anchor=west, color=FrontType]  {\textbf{?}};
    \node (A1) at (73.5,251) [anchor=west, color=FrontType]  {\textbf{?}};

    % Unknown gaps 
    \draw[color=black,line width=1.3, {Latex[length=2mm, width=2mm]}-{Latex[length=2mm, width=2mm]}]  (89.7,212.5) -- (89.7,225); % sector 6
    \draw[color=FrontSpeed,line width=1.3, {Latex[length=1.5mm, width=2mm]}-{Latex[length=1.5mm, width=2mm]}]  (106.6,211) -- (101.8304,211);

    % Labels
    \node (A1) at (45.0596,176.5) [anchor=west]   {\footnotesize GNL};
    \node (A2) at (70.6549,176.5) [anchor=west]     {\footnotesize  GNL};
    \node (A3) at  (101.5012,176.5) [anchor=west]  {\footnotesize  LD};
    \node (A4) at (124.2831,176.5) [anchor=west] {\footnotesize  GNL};

    % Legends
    \node (A1) at  (141,259) {$x$};
    \node (A1) at  (33,215) [rotate=90]{\footnotesize \textit{Solution profile}};

    \node (A1) at (-35,270)  [anchor=west, text width=25cm]  {\footnotesize GNL: Genuinely non-linear: undetermination of the wave type and link between the intermediate state values.\\[-0.5ex] LD: Linearly degenerated, contact  wave. The jump between the two  intermediate states is known, but  not their offset.\\[-0.5ex]};
    \node (A1) at  (-35,207) [anchor=west, color=FrontType]   {\textbf{?}};   \node (A1) at  (-30,207) [anchor=west, text width=6cm]  {\footnotesize Wave shape and front\\[-0.8ex] speed unknown (if applicable)};
    \node (A1) at  (-35,220) [anchor=west, color=IntermediateValue]  {\textbf{?}};  \node (A1) at   (-30,220) [anchor=west, text width=6cm]  {\footnotesize Intermediate state value\\[-0.8ex]  unknown};
    \node (A1) at  (-35,232) [anchor=west, color=FrontSpeed]   {\textbf{?}};  \node (A1) at  (-30,232) [anchor=west]  {\footnotesize Front speed unknown};

     % Axis
    \draw[color=black,line cap=butt,line join=miter,line width=1.9pt, <->] (36.3514,173.8543) -- (36.3514,255.5993) -- (36.3514,255.5993) -- (143.5608,255.5993);

    
\end{tikzpicture}
