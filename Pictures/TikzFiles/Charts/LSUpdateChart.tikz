\begin{tikzpicture}[y=0.80pt, x=0.80pt, yscale=-1, xscale=1, inner sep=0pt, outer sep=0pt]
  
    % Dashed arrow green
    \path[draw=StateArrowColor,dash pattern=on 7.20pt,miter limit=10.00,line width=2.400pt] (170.0000,390.0000) .. controls (170.0000,343.3333) and (198.3333,320.0000) .. (255.0000,320.0000) .. controls (311.6667,320.0000) and (340.0000,296.7000) .. (340.0000,250.1000);
    \path[draw=StateArrowColor,fill=StateArrowColor,miter limit=10.00,line width=2.400pt] (340.0000,243.3500) -- (344.5000,252.3500) -- (340.0000,250.1000) -- (335.5000,252.3500) -- cycle;
    % Top right curvy arrow
    \path[draw=black,miter limit=10.00,line width=2.400pt] (340.0000,200.0000) -- (340.0000,170.0000) .. controls (340.0000,163.3333) and (343.3333,160.0000) .. (350.0000,160.0000) -- (355.0000,160.0000) .. controls (358.3333,160.0000) and (360.0000,156.6667) .. (360.0000,150.0000) -- (360.0000,130.1000);
    \path[draw=black,fill=black,miter limit=10.00,line width=2.400pt] (360.0000,123.3500) -- (364.5000,132.3500) -- (360.0000,130.1000) -- (355.5000,132.3500) -- cycle;
    % Top right straight arrow  
    \path[draw=black,miter limit=10.00,line width=2.400pt] (400.0000,80.0000) -- (400.0000,60.1000);
    \path[draw=black,fill=black,miter limit=10.00,line width=2.400pt] (400.0000,53.3500) -- (404.5000,62.3500) -- (400.0000,60.1000) -- (395.5000,62.3500) -- cycle;
    %Top left straight arrow
    \path[draw=black,miter limit=10.00,line width=2.400pt] (130.0000,80.0000) -- (130.0000,60.1000);
    \path[draw=black,fill=black,miter limit=10.00,line width=2.400pt] (130.0000,53.3500) -- (134.5000,62.3500) -- (130.0000,60.1000) -- (125.5000,62.3500) -- cycle;
    % Straight big left arrow
    \path[draw=black,miter limit=10.00,line width=2.400pt] (90.0000,390.0000) -- (90.0000,170.1000);
    \path[draw=black,fill=black,miter limit=10.00,line width=2.400pt] (90.0000,163.3500) -- (94.5000,172.3500) -- (90.0000,170.1000) -- (85.5000,172.3500) -- cycle;
    % Straight big right arrow
    \path[draw=black,miter limit=10.00,line width=2.400pt] (423.0000,390.0000) -- (423.0000,130.1000);
    \path[draw=black,fill=black,miter limit=10.00,line width=2.400pt] (423.0000,123.3500) -- (427.5000,132.3500) -- (423.0000,130.1000) -- (418.5000,132.3500) -- cycle;
    % Curvy middle blue arrow  
    \path[draw=SubdomainArrowColor,dash pattern=on 7.20pt,miter limit=10.00,line width=2.400pt,xshift=-0.2em,yshift=-0.03em] (370.0000,390.0000) .. controls (370.0000,343.3333) and (340.0000,320.0000) .. (280.0000,320.0000) .. controls (220.0000,320.0000) and (190.0000,296.7000) .. (190.0000,250.1000);
    \path[draw=SubdomainArrowColor,fill=SubdomainArrowColor,miter limit=10.00,line width=2.400pt,xshift=-0.2em] (190.0000,243.3500) -- (194.5000,252.3500) -- (190.0000,250.1000) -- (185.5000,252.3500) -- cycle;
    % Middle left curvy arrow
    \path[draw=black,miter limit=10.00,line width=2.400pt] (190.0000,200.0000) -- (190.0000,195.0000) .. controls (190.0000,191.6667) and (186.6667,190.0000) .. (180.0000,190.0000) -- (140.0000,190.0000) .. controls (133.3333,190.0000) and (130.0000,186.6833) .. (130.0000,180.0500) -- (130.0000,170.1000);
    \path[draw=black,fill=black,miter limit=10.00,line width=2.400pt] (130.0000,163.3500) -- (134.5000,172.3500) -- (130.0000,170.1000) -- (125.5000,172.3500) -- cycle;
    % Straight very right big arrow
    \path[draw=black,miter limit=10.00,line width=2.400pt,xshift=-2em] (570.0000,410.0000) -- (570.0000,230.0000) .. controls (570.0000,223.3333) and (570.0000,216.6667) .. (570.0000,210.0000) -- (570.0000,40.1000);
    \path[draw=black,miter limit=10.00,line width=2.400pt,xshift=-2em] (565.0000,405.0000) -- (575.0000,405.0000);
    \path[draw=black,fill=black,miter limit=10.00,line width=2.400pt,xshift=-2em] (570.0000,33.3500) -- (574.5000,42.3500) -- (570.0000,40.1000) -- (565.5000,42.3500) -- cycle;
    % Horizontal line dashed
    \path[draw=black,dash pattern=on 2.40pt,miter limit=10.00] (0.0000,180.0000) -- (565.0000,180.0000);

    % Boxes
    \path[draw=none,fill=StateColor,rounded corners=0.1355cm] (295.0000,200.0000) rectangle (385.0000,240.0000); % Velocity extraction box
    \path[draw=none,fill=StateColor,rounded corners=0.5cm] (50.0000,120.0000) rectangle (210.0000,160.0000);    % EoSbox 
    \path[draw=none,fill=StateColor,rounded corners=0.5cm] (50.0000,80.0000) rectangle (210.0000,120.0000);   % State update box
    \path[draw=none,fill=SubdomainColor,rounded corners=0.5cm] (320.0000,80.0000) rectangle (480.0000,120.0000);      % Interface evolution box
    \path[draw=none,fill=SubdomainColor,rounded corners=0.1355cm]  (105.0000,200.0000) rectangle (275.0000,240.0000);  % Fluid selection box
    \path[draw=none,fill=VeryLightGray,rounded corners=0.1355cm] (330.0000,390.0000) rectangle (490.0000,430.0000);     % Initial interface box
    \path[draw=none,fill=VeryLightGray,rounded corners=0.1355cm] (50.0000,390.0000) rectangle (210.0000,430.0000);     % Initial state box
    \path[draw=none,fill=LightGray,rounded corners=0.1355cm] (50.0000,10.0000) rectangle (210.0000,50.0000);       % Final state box
    \path[draw=none,fill=LightGray,rounded corners=0.1355cm] (300.0000,10.0000) rectangle (500.0000,50.0000);      % Final interface box

    % Top and bottom big boxes
    \path[draw=black,rounded corners=0.2032cm,line width=1.2pt] (40.0000,0.0000) rectangle (510.0000,60.0000);
    \path[draw=black,rounded corners=0.2032cm,line width=1.2pt] (40.0000,380.0000) rectangle (500.0000,440.0000);

    % Labels
    \node[inner sep=1.5ex,text width=150, align=center] at (400,30.0000) {\footnotesize Updated\\[-0.35em] interface representation};  
    \node[inner sep=1.5ex,text width=70, align=center, ] at (340,220) {\footnotesize  Velocity field\\[-0.3em] extraction};  
    \node[inner sep=1.5ex,text width=150, align=center, ] at (131,99) {\footnotesize  System dynamics};  
    \node[inner sep=1.5ex,text width=150, align=center, ] at (131,141) {\footnotesize  Equation of State};  
    \node[inner sep=1.5ex,text width=150, align=center, ] at (129,411) {\footnotesize  Previous state $\Y^{n}$};  
    \node[inner sep=1.5ex,text width=150, align=center, ] at (412,411) {\footnotesize  Interface representation};  
    \node[inner sep=1.5ex,text width=150, align=center] at (132,30.0000) {\footnotesize Updated state $\Y^{n+1}$};  
    \node[inner sep=1.5ex,text width=30, align=left ] at (570,405) {\small  $t^n$};  
    \node[inner sep=1.5ex,text width=30, align=left ] at (570,30) {\small  $t^{n+1}$};
    \node[inner sep=1.5ex,text width=150, align=center, rotate=-90 ] at (555,115) {\footnotesize Decoupled update};  
    \node[inner sep=1.5ex,text width=150, align=center, rotate=-90 ] at (555,283) {\footnotesize Coupled static precomputations};  
    \node[inner sep=1.5ex,text width=150, align=center] at (400,99.0000) {\footnotesize Interface evolution};  
    \node[inner sep=1.5ex,text width=150, align=center] at (191,220.0000) {\footnotesize Interface location\\[-0.3em] Subdomains determination};
\end{tikzpicture}

