%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% A casual Thesis template for almost blind people  %% 
%%                                                   %% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% =====================================================================================
%									Packages import
% =====================================================================================
% Initialise the class
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{Tools/ThesisStyle}[Thesis Latex Class]
\LoadClass[a4paper,fleqn, 12pt,final,openright,twoside]{report}

% Multiple fonts compatibility tweak
\newcommand{\bmmax}{0}
\newcommand{\hmmax}{0}

% Geometry and margins
\RequirePackage[top=3cm, bottom=3cm, left=3cm, right=3cm]{geometry}

% Input and langages
\RequirePackage[euler]{textgreek}
\RequirePackage[utf8]{inputenc}
\RequirePackage[english]{babel}
\RequirePackage{upgreek}

% Formatting
\RequirePackage[final]{microtype}
\RequirePackage{hyphenat}
\RequirePackage{csquotes}
\RequirePackage{multicol}
\RequirePackage{placeins}

% References
\RequirePackage[citestyle=numeric,bibstyle=ieee,doi=false,isbn=false,url=false,eprint=false]{biblatex}

% Mathy nerdy packages
\RequirePackage{mathtools}
\RequirePackage{stmaryrd}
\RequirePackage{mathrsfs}
\RequirePackage{enumitem}
\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{amsthm}
\RequirePackage{bbold}
\RequirePackage{cases}
\RequirePackage{bbm}
\RequirePackage{bm}

% Reader-friendly packages
\RequirePackage{setspace}
\RequirePackage{caption}

% Header, footers and etc
\RequirePackage{fancyhdr}
\RequirePackage{shorttoc}
\RequirePackage{tocloft}
\RequirePackage[tight]{minitoc}
\RequirePackage[explicit]{titlesec}

% Color scheme and visualisation
\RequirePackage[table]{xcolor}
\let\normalcolor\relax

% Graphical handling
\RequirePackage[many,breakable]{tcolorbox}
\RequirePackage{adjustbox}
\RequirePackage{pgfplots}
\RequirePackage{graphicx}
\RequirePackage{pgfplots}
\RequirePackage{rotating}
\RequirePackage{boldline}
\RequirePackage{wrapfig}
\RequirePackage{subfig}
\RequirePackage{float}
\RequirePackage{array}
\RequirePackage{tikz}
\input{insbox.tex}


% =====================================================================================
%						Pathes
% =====================================================================================
% Input hacks
\input{Tools/LatexHacks}

% Location of pictures
\graphicspath{{Pictures/}{Icons/}{NumericalResults/}{Pictures/PNGFiles}}

% =====================================================================================
%						Theming of the contents
% =====================================================================================
%\renewcommand\normalsize{%
%	\@setfontsize\normalsize\@xipt{13.6}%
%	\abovedisplayskip 14\p@ \@plus3\p@ \@minus6\p@
%	\abovedisplayshortskip \z@ \@plus3\p@
%	\belowdisplayshortskip 8.5\p@ \@plus3.5\p@ \@minus3\p@
%	\belowdisplayskip \abovedisplayskip
%	\let\@listi\@listI}

% =====================================================================================
%						Theming of the contents
% =====================================================================================

% -------------------------- Fonts ----------------------------------------------------
% Font schemes
\renewcommand{\familydefault}{pnc}			% Abcd font
%\RequirePackage{fouriernc}	                % Mathy font
%\let\temp\rmdefault\usepackage{mathpazo}\let\rmdefault\temp

% -------------------------- Hyperlinks -----------------------------------------------
\RequirePackage[bookmarks=true, bookmarksnumbered=true, bookmarksopen=true, unicode=true, colorlinks=true,
linktoc=all, linkcolor=TextColor, citecolor=TextColor, filecolor=TextColor, urlcolor=TextColor,
pdfstartview=FitH]{hyperref}

% -------------------------- Color schemes --------------------------------------------
% Colouring scheme
\definecolor{DarkColor}{gray}{0.75}
\definecolor{LightColor}{gray}{0.9}
\definecolor{LightGrey}{rgb}{0.96,0.96,0.96}
\definecolor{DarkGrey}{rgb}{0.83,0.83,0.83}
\definecolor{BaseColor}{rgb}{0.12,0.25,0.47}
\definecolor{BASECOLOR}{rgb}{0.12,0.25,0.47}
\definecolor{TextColor}{rgb}{0.12,0.25,0.47}

% Bounding boxes plain
\colorlet{DefBox}{BaseColor!2!white}
\colorlet{TheoBox}{BaseColor!7!white}
\colorlet{AssumpBox}{BaseColor!7!white}
\colorlet{PropBox}{BaseColor!7!white}

% Bounding boxes titles 
\colorlet{DefBoxTitle}{BaseColor!30!white}
\colorlet{TheoBoxTitle}{BaseColor!30!white}
\colorlet{AssumpBoxTitle}{BaseColor!30!white}
\colorlet{PropBoxTitle}{BaseColor!30!white}
\colorlet{PropBoxTitle}{BaseColor!30!white}

% Simple frames
\colorlet{FrameCol}{BaseColor!50!white}


% =====================================================================================
%						Floats formatting
% =====================================================================================
% --------------- Custom counters ------------------------------------------------- 
\newfloat{Algorithm}{htbp}{loa}

% --------------- Tables formatting ---------------------------------------------------
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}
\makeatletter\def\hlinewd#1{\noalign{\ifnum0<=`}\fi\hrule \@height #1\futurelet\reserved@a\@xhline}\makeatother
\newcommand{\thickhline}{\hlineB{4}}
\arrayrulecolor{BaseColor}
\arrayrulewidth = 1.5pt

% --------------- List formatting ---------------------------------------------------
\renewcommand{\labelitemi}{\textbullet}

% --------------- Figures formatting ------------------------------------------------
% Automatic figure size
\newlength{\figurewidth}

% Caption formatting
\captionsetup{
	labelfont={color=TextColor, bf, small},      	% font(sf), color, bold, size of the caption label
	textfont={small},								% font(sf) and size of the caption text itself
	labelsep=period,								% separator between label and text
	margin=10mm}								    % left/right margins

% Equation numbering formatting
\makeatletter
\newcommand{\RefRange}[2]{{\color{TextColor}\textbf{(\ref{#1}--\ref{#2})}} }
\newcommand{\RefRanges}[2]{{\color{TextColor}\textbf{(\ref{#1}--\ref{#2})}}}
\makeatother

\makeatletter\def\tagform@#1{\maketag@@@{\color{TextColor}\textbf{(#1)}\normalcolor\@@italiccorr}}\makeatother

% --------------- Math environment formatting -----------------------------------------
% Set mathy problematics
\renewcommand{\qedsymbol}{$\blacksquare$}


% =====================================================================================
%						Heavy math tricks
% =====================================================================================
\makeatletter
\newsavebox\myboxA
\newsavebox\myboxB
\newlength\mylenA
\newcommand*\xoverline[2][0.75]{%
	\sbox{\myboxA}{$\m@th#2$}%
	\setbox\myboxB\null% Phantom box
	\ht\myboxB=\ht\myboxA%
	\dp\myboxB=\dp\myboxA%
	\wd\myboxB=#1\wd\myboxA% Scale phantom
	\sbox\myboxB{$\m@th\overline{\copy\myboxB}$}%  Overlined phantom
	\setlength\mylenA{\the\wd\myboxA}%   calc width diff
	\addtolength\mylenA{-\the\wd\myboxB}%
	\ifdim\wd\myboxB<\wd\myboxA%
	\rlap{\hskip 1\mylenA\usebox\myboxB}{\usebox\myboxA}%
	\else
	\hskip -1\mylenA\rlap{\usebox\myboxA}{\hskip 1\mylenA\usebox\myboxB}%
	\fi}
\makeatother


% =====================================================================================
%		Shortcutting the mathy sectionning and styling them
% =====================================================================================

% ---------------  Definition styling ------------------------------------------------- 
\makeatletter
\renewcommand{\new@tcbtheorem}[6][]{%
	\@@newtcolorbox[auto counter,#1]{#2}[3][]{#4,%
		title={\tcb@theo@title{{\hspace{-0.5em}
					\begin{tcolorbox}[colback=#6,size=fbox,hbox,frame hidden, boxrule=0pt, sharp corners=northwest, arc=2mm, tcbox raise = 2.4pt]{{\sffamily\fontseries{b}\phantom{I}#3 \thetcbcounter\phantom{q}}}\end{tcolorbox}}}
			  {\space}
			  {\begin{tcolorbox}[colback=DefBox,sharp corners,hbox,size=fbox, frame hidden, boxrule=0pt,tcbox raise = 2pt]{\hspace{-0.5em}\textit{\fontseries{m}\selectfont##2}}\end{tcolorbox}}},%
		list entry={\protect\numberline{\thetcbcounter}##2},nameref={##2},code={\tcb@theo@label{#5}{##3}},##1}}
\makeatother

\newtcbtheorem[number within=section]{definition}{Definition}{
	enhanced jigsaw,sharp corners =northwest,size=small,skin=bicolor,breakable,
	colframe=FrameCol,colback=DefBox,colbacklower=DefBox,frame hidden,
	separator sign none, clip upper,top=7mm,pad at break=1mm,arc=3mm,
	detach title,description delimiters none,description color=DefBoxTitle,before
	skip=10pt,after skip=10pt,
	overlay={\draw[thick,FrameCol,rounded corners=3mm](interior.north west)--( interior.south west)-- ([xshift=-3mm] interior.south east);\node[below right, xshift=-2.2pt, yshift=4.5pt] at (interior.north west) {\tcbtitle};\draw[thick,FrameCol,rounded corners=3mm] ([xshift=3mm] interior.south west)-- (interior.south east) -- (interior.north east)--(interior.north west);},
	overlay first={\draw[thick,FrameCol,rounded corners=3mm](interior.north west)--( interior.south west);
		           \node[below right, xshift=-2.2pt, yshift=4.5pt] at (interior.north west) {\tcbtitle};
		           \draw[thick,FrameCol,rounded corners=3mm] (interior.south east)-- (interior.north east) -- (interior.north west);},
	overlay middle={\draw[thick,FrameCol,rounded corners=3mm](interior.north west)--( interior.south west);
		            \draw[thick,FrameCol,rounded corners=3mm] (interior.south east)-- (interior.north east);},
	overlay last={\draw[thick,FrameCol,rounded corners=3mm] (interior.north west)-- (interior.south west) -- (interior.south east) -- (interior.north east);},	
}{def}{DefBoxTitle}


% -------------------- Admissibility conditions styling ------------------------------------------------- 
\makeatletter
\renewcommand{\new@tcbtheorem}[6][]{%
	\@@newtcolorbox[auto counter,#1]{#2}[3][]{#4,%
		title={\tcb@theo@title{{\hspace{-0.5em}
					\begin{tcolorbox}[colback=#6,size=fbox,hbox,frame hidden, boxrule=0pt, sharp corners=northwest, arc=2mm, tcbox raise = 2.4pt]{{\sffamily\fontseries{b}\phantom{I}#3 \thetcbcounter\phantom{q}}}\end{tcolorbox}}}
			{\space}
			{\begin{tcolorbox}[colback=DefBox,sharp corners,hbox,size=fbox, frame hidden, boxrule=0pt,tcbox raise = 2pt]{\hspace{-0.5em}\textit{\fontseries{m}\selectfont##2}}\end{tcolorbox}}},%
		list entry={\protect\numberline{\thetcbcounter}##2},nameref={##2},code={\tcb@theo@label{#5}{##3}},##1}}
\makeatother

\newtcbtheorem[number within=section]{admissibility}{Admissibility conditions}{
	enhanced jigsaw,sharp corners =northwest,size=small,skin=bicolor,breakable,
	colframe=FrameCol,colback=DefBox,colbacklower=DefBox,frame hidden,
	separator sign none, clip upper,top=7mm,pad at break=1mm,arc=3mm,
	detach title,description delimiters none,description color=DefBoxTitle,before
	skip=10pt,after skip=10pt,
	overlay={\draw[thick,FrameCol,rounded corners=3mm](interior.north west)--( interior.south west)-- ([xshift=-3mm] interior.south east);\node[below right, xshift=-2.2pt, yshift=4.5pt] at (interior.north west) {\tcbtitle};\draw[thick,FrameCol,rounded corners=3mm] ([xshift=3mm] interior.south west)-- (interior.south east) -- (interior.north east)--(interior.north west);},
	overlay first={\draw[thick,FrameCol,rounded corners=3mm](interior.north west)--( interior.south west);
		\node[below right, xshift=-2.2pt, yshift=4.5pt] at (interior.north west) {\tcbtitle};
		\draw[thick,FrameCol,rounded corners=3mm] (interior.south east)-- (interior.north east) -- (interior.north west);},
	overlay middle={\draw[thick,FrameCol,rounded corners=3mm](interior.north west)--( interior.south west);
		\draw[thick,FrameCol,rounded corners=3mm] (interior.south east)-- (interior.north east);},
	overlay last={\draw[thick,FrameCol,rounded corners=3mm] (interior.north west)-- (interior.south west) -- (interior.south east) -- (interior.north east);},	
}{adm}{DefBoxTitle}


% --------------------------  Theorem styling ------------------------------------------------- 
\makeatletter
\renewcommand{\new@tcbtheorem}[6][]{%
	\@@newtcolorbox[auto counter,#1]{#2}[3][]{#4,%
		title={\tcb@theo@title{
				{\hspace{-0.5em}\begin{tcolorbox}[colback=#6,size=fbox,hbox, frame hidden, colframe = FrameCol, boxrule=0pt, sharp corners=northwest, arc=2mm, tcbox raise = 2.4pt]{{\sffamily\fontseries{b}\selectfont \phantom{I}#3 \thetcbcounter\phantom{q}}}\end{tcolorbox}}}
			{\space}
			{\begin{tcolorbox}[colback=TheoBox,sharp corners,hbox,size=fbox, frame hidden, boxrule=0pt,tcbox raise = 1.8pt]{\hspace{-0.5em}\textcolor{black}{\fontseries{b}\fontshape{it}\selectfont ##2}}\end{tcolorbox}}},%
		list entry={\protect\numberline{\thetcbcounter}##2},nameref={##2},code={\tcb@theo@label{#5}{##3}},##1}}
\makeatother

\newtcbtheorem[number within=section]{theo}{Theorem}{%
	enhanced jigsaw,rounded corners,size=small,skin=bicolor,breakable,
	colframe=FrameCol,colback=TheoBox,colbacklower=white, separator sign none,
	clip upper,detach title,description delimiters none,description color=TheoBoxTitle,
	top=7mm,pad at break=1mm,frame hidden,arc=3mm,before
	skip=10pt,after skip=10pt,
	overlay={\draw[thick,FrameCol,rounded corners=3mm] (interior.north west)--( interior.south west)--( [xshift=3mm] interior.south west);\node[below right, xshift=-2.4pt, yshift=4.2pt] at (interior.north west) {\tcbtitle};},
	overlay first={\draw[thick,FrameCol] (interior.north west)--( interior.south west);
		           \node[below right, xshift=-2.4pt, yshift=4.2pt] at (interior.north west) {\tcbtitle};},
	overlay middle={\draw[thick,FrameCol] (interior.north west)--( interior.south west);},
	overlay last={\draw[thick,FrameCol,rounded corners=3mm] (interior.north west)--( interior.south west)--( [xshift=3mm] interior.south west);},	
}{th}{TheoBoxTitle}

\newtcbtheorem[number within=section]{theoproof}{Theorem}{%
	enhanced jigsaw,rounded corners,size=small,skin=bicolor,breakable,
	colframe=FrameCol,colback=TheoBox,colbacklower=white,separator sign none, 
	clip upper,detach title,description delimiters none,description color=TheoBoxTitle,
	top=7mm,pad at break=1mm,frame hidden,arc=3mm,before
	skip=10pt,after skip=10pt,
	overlay ={\node[below right, xshift=-2.4pt, yshift=4.2pt] at (interior.north west) {\tcbtitle};
		      \draw[thick,FrameCol,rounded corners=1ex] (interior.north west)--(interior.south west)--(interior.south east);\node[FrameCol, xshift=-5pt, yshift=10pt] at (interior.south east) {$\blacksquare$};},
	overlay first ={\draw[thick,FrameCol,rounded corners=1ex] (interior.north west)--(interior.south west);
		            \node[below right, xshift=-2.4pt, yshift=4.2pt] at (interior.north west) {\tcbtitle};},
	overlay middle={\draw[thick,FrameCol,rounded corners=1ex] (interior.north west)--(interior.south west);},
    overlay last={\draw[thick,FrameCol,rounded corners=1ex] (interior.north west)--(interior.south west)--(interior.south east);
				  \node[FrameCol, xshift=-5pt, yshift=10pt] at (interior.south east) {$\blacksquare$};},
}{th}{TheoBoxTitle}

% ----------------------------  Assumption styling ------------------------------------------------- 
\makeatletter
\renewcommand{\new@tcbtheorem}[6][]{%
	\@@newtcolorbox[auto counter,#1]{#2}[3][]{#4,%
		title={\tcb@theo@title{
				{\hspace{-0.5em}\begin{tcolorbox}[colback=#6,size=fbox,hbox, frame hidden, colframe = BaseColor, boxrule=0pt,  sharp corners=northwest, arc=2mm, tcbox raise = 2.4pt]{{\sffamily\fontseries{b}\selectfont \phantom{I}#3 \thetcbcounter\phantom{q}}}\end{tcolorbox}}}
			{\space}
			{\begin{tcolorbox}[colback=AssumpBox,sharp corners,hbox,size=fbox, frame hidden, boxrule=0pt,tcbox raise = 2pt]{\hspace{-0.5em}\textcolor{black}{{{\fontseries{b}\fontshape{it}\selectfont ##2}}}}\end{tcolorbox}}},%
		list entry={\protect\numberline{\thetcbcounter}##2},nameref={##2},code={\tcb@theo@label{#5}{##3}},##1}}
\makeatother

\newtcbtheorem[number within=section]{assumption}{Assumption}{%
	enhanced jigsaw,rounded corners,size=small,skin=bicolor,breakable,
	colframe=FrameCol,colback=AssumpBox,colbacklower=white,
	separator sign none,before
	skip=10pt,after skip=10pt,
	clip upper,detach title,description delimiters none,description color=AssumpBoxTitle,
	top=7mm,pad at break=1mm,frame hidden,arc=3mm,
	overlay={\draw[thick,FrameCol,rounded corners=3mm](interior.north west)--( interior.south west)-- ([xshift=-3mm] interior.south east);\node[below right, xshift=-2.2pt, yshift=4.2pt] at (interior.north west) {\tcbtitle};\draw[thick,FrameCol,rounded corners=3mm] ([xshift=3mm] interior.south west)-- (interior.south east) -- (interior.north east)--(interior.north west);},
	overlay first={\draw[thick,FrameCol,rounded corners=3mm](interior.north west)--( interior.south west);
		\node[below right, xshift=-2.2pt, yshift=4.2pt] at (interior.north west) {\tcbtitle};
		\draw[thick,FrameCol,rounded corners=3mm] (interior.south east)-- (interior.north east) -- (interior.north west);},
	overlay middle={\draw[thick,FrameCol,rounded corners=3mm](interior.north west)--( interior.south west);
		\draw[thick,FrameCol,rounded corners=3mm] (interior.south east)-- (interior.north east);},
	overlay last={\draw[thick,FrameCol,rounded corners=3mm] (interior.north west)-- (interior.south west) -- (interior.south east) -- (interior.north east);},	
}{as}{AssumpBoxTitle}

% -------------------------------  Property styling ------------------------------------------------- 
\makeatletter
\renewcommand{\new@tcbtheorem}[6][]{%
	\@@newtcolorbox[auto counter,#1]{#2}[3][]{#4,%
		title={\tcb@theo@title{
				{\hspace{-0.5em}\begin{tcolorbox}[colback=#6,size=fbox,hbox, frame hidden, colframe = FrameCol, boxrule=0pt,  sharp corners=northwest, arc=2mm, tcbox raise = 2.4pt]{{\sffamily\fontseries{b}\selectfont \phantom{I}#3 \thetcbcounter\phantom{q}}}\end{tcolorbox}}}
			{\space}
			{\begin{tcolorbox}[colback=PropBox,sharp corners,hbox,size=fbox, frame hidden, boxrule=0pt,tcbox raise = 2pt]{\hspace{-0.5em}\textcolor{black}{{{\fontseries{b}\fontshape{it}\selectfont ##2}}}}\end{tcolorbox}}},%
		list entry={\protect\numberline{\thetcbcounter}##2},nameref={##2},code={\tcb@theo@label{#5}{##3}},##1}}
\makeatother

\newtcbtheorem[number within=section]{property}{Property}{%
	enhanced jigsaw,rounded corners,size=small,skin=bicolor,breakable,
	colframe=FrameCol,colback=PropBox,colbacklower=white,
	separator sign none,
	clip upper,detach title,description delimiters none,description color=BaseColor,
	top=7mm,pad at break=1mm,frame hidden,arc=3mm,before
	skip=10pt,after skip=10pt,
	overlay={\draw[thick,FrameCol,rounded corners=3mm] (interior.north west)--( interior.south west)--( [xshift=3mm] interior.south west);\node[below right, xshift=-2.4pt, yshift=4.5pt] at (interior.north west) {\tcbtitle};},
	overlay first={\draw[thick,FrameCol] (interior.north west)--( interior.south west);
		\node[below right, xshift=-2.4pt, yshift=4.5pt] at (interior.north west) {\tcbtitle};},
	overlay middle={\draw[thick,FrameCol] (interior.north west)--( interior.south west);},
	overlay last={\draw[thick,FrameCol,rounded corners=3mm] (interior.north west)--( interior.south west)--( [xshift=3mm] interior.south west);},	
}{property}{PropBoxTitle}

\newtcbtheorem[number within=section]{propertyproof}{Property}{%
	enhanced jigsaw,rounded corners,size=small,skin=bicolor,breakable,
	colframe=FrameCol,colback=TheoBox,colbacklower=white,separator sign none, 
	clip upper,detach title,description delimiters none,description color=TheoBoxTitle,
	top=7mm,pad at break=1mm,frame hidden,arc=3mm,before
	skip=10pt,after skip=10pt,
	overlay ={\node[below right, xshift=-2.4pt, yshift=4.2pt] at (interior.north west) {\tcbtitle};
		\draw[thick,FrameCol,rounded corners=1ex] (interior.north west)--(interior.south west)--(interior.south east);\node[FrameCol, xshift=-5pt, yshift=10pt] at (interior.south east) {$\blacksquare$};},
	overlay first ={\draw[thick,FrameCol,rounded corners=1ex] (interior.north west)--(interior.south west);
		\node[below right, xshift=-2.4pt, yshift=4.2pt] at (interior.north west) {\tcbtitle};},
	overlay middle={\draw[thick,FrameCol,rounded corners=1ex] (interior.north west)--(interior.south west);},
	overlay last={\draw[thick,FrameCol,rounded corners=1ex] (interior.north west)--(interior.south west)--(interior.south east);
		\node[FrameCol, xshift=-5pt, yshift=10pt] at (interior.south east) {$\blacksquare$};},
}{property}{TheoBoxTitle}

% -------------------------------  Proposition styling ------------------------------------------------- 
\makeatletter
\renewcommand{\new@tcbtheorem}[6][]{%
	\@@newtcolorbox[auto counter,#1]{#2}[3][]{#4,%
		title={\tcb@theo@title{
				{\hspace{-0.5em}\begin{tcolorbox}[colback=#6,size=fbox,hbox, frame hidden, colframe = FrameCol, boxrule=0pt,  sharp corners=northwest, arc=2mm, tcbox raise = 2.4pt]{{\sffamily\fontseries{b}\selectfont \phantom{I}#3 \thetcbcounter\phantom{q}}}\end{tcolorbox}}}
			{\space}
			{\begin{tcolorbox}[colback=PropBox,sharp corners,hbox,size=fbox, frame hidden, boxrule=0pt,tcbox raise = 2pt]{\hspace{-0.5em}\textcolor{black}{{{\fontseries{b}\fontshape{it}\selectfont ##2}}}}\end{tcolorbox}}},%
		list entry={\protect\numberline{\thetcbcounter}##2},nameref={##2},code={\tcb@theo@label{#5}{##3}},##1}}
\makeatother

\newtcbtheorem[number within=section]{prop}{Proposition}{%
	enhanced jigsaw,rounded corners,size=small,skin=bicolor,breakable,
	colframe=FrameCol,colback=PropBox,colbacklower=white,
	separator sign none,
	clip upper,detach title,description delimiters none,description color=BaseColor,
	top=7mm,pad at break=1mm,frame hidden,arc=3mm,before
	skip=10pt,after skip=10pt,
	overlay={\draw[thick,FrameCol,rounded corners=3mm] (interior.north west)--( interior.south west)--( [xshift=3mm] interior.south west);\node[below right, xshift=-2.4pt, yshift=4.5pt] at (interior.north west) {\tcbtitle};},
	overlay first={\draw[thick,FrameCol] (interior.north west)--( interior.south west);
		\node[below right, xshift=-2.4pt, yshift=4.5pt] at (interior.north west) {\tcbtitle};},
	overlay middle={\draw[thick,FrameCol] (interior.north west)--( interior.south west);},
	overlay last={\draw[thick,FrameCol,rounded corners=3mm] (interior.north west)--( interior.south west)--( [xshift=3mm] interior.south west);},	
}{pr}{PropBoxTitle}


% ---------------------------------------- Corollary styling ------------------------------------------------- 
\newtcbtheorem[number within=section]{corollary}{Corollary}{%
	enhanced jigsaw,rounded corners,size=small,skin=bicolor,breakable,
	colframe=FrameCol,colback=PropBox,colbacklower=white,
	separator sign none,
	clip upper,detach title,description delimiters none,description color=BaseColor,
	top=7mm,pad at break=1mm,frame hidden,arc=3mm,before
	skip=10pt,after skip=10pt,
	overlay={\draw[thick,FrameCol,rounded corners=3mm] (interior.north west)--( interior.south west)--( [xshift=3mm] interior.south west);\node[below right, xshift=-2.4pt, yshift=4.5pt] at (interior.north west) {\tcbtitle};},
	overlay first={\draw[thick,FrameCol] (interior.north west)--( interior.south west);
		\node[below right, xshift=-2.4pt, yshift=4.5pt] at (interior.north west) {\tcbtitle};},
	overlay middle={\draw[thick,FrameCol] (interior.north west)--( interior.south west);},
	overlay last={\draw[thick,FrameCol,rounded corners=3mm] (interior.north west)--( interior.south west)--( [xshift=3mm] interior.south west);},	
}{coro}{PropBoxTitle}

\newtcbtheorem[number within=section]{coroproof}{Corollary}{%
	enhanced jigsaw,rounded corners,size=small,skin=bicolor,breakable,
	colframe=FrameCol,colback=PropBox,colbacklower=white,
	separator sign none,clip upper,detach title,description delimiters none,description color=PropBoxTitle,
	top=7mm,pad at break=1mm,frame hidden,arc=3mm,before
	skip=10pt,after skip=10pt,
	overlay ={\node[below right, xshift=-2.4pt, yshift=4.5pt] at (interior.north west) {\tcbtitle};
		\draw[thick,FrameCol,rounded corners=1ex] (interior.north west)--(interior.south west)--(interior.south east);\node[FrameCol, xshift=-5pt, yshift=10pt] at (interior.south east) {$\blacksquare$};},
	overlay first ={\draw[thick,FrameCol,rounded corners=1ex] (interior.north west)--(interior.south west);
		\node[below right, xshift=-2.4pt, yshift=4.5pt] at (interior.north west) {\tcbtitle};},
	overlay middle={\draw[thick,FrameCol,rounded corners=1ex] (interior.north west)--(interior.south west);},
	overlay last={\draw[thick,FrameCol,rounded corners=1ex] (interior.north west)--(interior.south west)--(interior.south east);
		\node[FrameCol, xshift=-5pt, yshift=10pt] at (interior.south east) {$\blacksquare$};},
}{coro}{PropBoxTitle}

% ---------------------------------------- Lemma styling ------------------------------------------------- 
\newtcbtheorem[number within=section]{lemmaproof}{Lemma}{%
	enhanced jigsaw,rounded corners,size=small,skin=bicolor,breakable,
	colframe=FrameCol,colback=PropBox,colbacklower=white,
	separator sign none,clip upper,detach title,description delimiters none,description color=PropBoxTitle,
	top=7mm,pad at break=1mm,frame hidden,arc=3mm,before
	skip=10pt,after skip=10pt,
	overlay ={\node[below right, xshift=-2.4pt, yshift=4.5pt] at (interior.north west) {\tcbtitle};
		\draw[thick,FrameCol,rounded corners=1ex] (interior.north west)--(interior.south west)--(interior.south east);\node[FrameCol, xshift=-5pt, yshift=10pt] at (interior.south east) {$\blacksquare$};},
	overlay first ={\draw[thick,FrameCol,rounded corners=1ex] (interior.north west)--(interior.south west);
		\node[below right, xshift=-2.4pt, yshift=4.5pt] at (interior.north west) {\tcbtitle};},
	overlay middle={\draw[thick,FrameCol,rounded corners=1ex] (interior.north west)--(interior.south west);},
	overlay last={\draw[thick,FrameCol,rounded corners=1ex] (interior.north west)--(interior.south west)--(interior.south east);
		\node[FrameCol, xshift=-5pt, yshift=10pt] at (interior.south east) {$\blacksquare$};},
}{lm}{PropBoxTitle}

\newtcbtheorem[number within=section]{propproof}{Proposition}{%
	enhanced jigsaw,rounded corners,size=small,skin=bicolor,breakable,
	colframe=FrameCol,colback=PropBox,colbacklower=white,
	separator sign none,clip upper,detach title,description delimiters none,description color=PropBoxTitle,
	top=7mm,pad at break=1mm,frame hidden,arc=3mm,before
	skip=10pt,after skip=10pt,
	overlay ={\node[below right, xshift=-2.4pt, yshift=4.5pt] at (interior.north west) {\tcbtitle};
		\draw[thick,FrameCol,rounded corners=1ex] (interior.north west)--(interior.south west)--(interior.south east);\node[FrameCol, xshift=-5pt, yshift=10pt] at (interior.south east) {$\blacksquare$};},
	overlay first ={\draw[thick,FrameCol,rounded corners=1ex] (interior.north west)--(interior.south west);
		\node[below right, xshift=-2.4pt, yshift=4.5pt] at (interior.north west) {\tcbtitle};},
	overlay middle={\draw[thick,FrameCol,rounded corners=1ex] (interior.north west)--(interior.south west);},
	overlay last={\draw[thick,FrameCol,rounded corners=1ex] (interior.north west)--(interior.south west)--(interior.south east);
		\node[FrameCol, xshift=-5pt, yshift=10pt] at (interior.south east) {$\blacksquare$};},
}{pr}{PropBoxTitle}

% ---------------------------------------- Proof styling ------------------------------------------------- 
\makeatletter
\renewcommand{\new@tcbtheorem}[6][]{%
	\@@newtcolorbox[auto counter,#1]{#2}[3][]{#4,%
		title={\tcb@theo@title{\emph{Proof of ##2}}{\space}{$ $}},%
		list entry={\protect\numberline{\thetcbcounter}##2},%
		nameref={##2},%
		code={\tcb@theo@label{#5}{##3}},%
		##1}}
\makeatother
	
\renewtcbtheorem[number within=section]{proof}{Proof}{%
	enhanced jigsaw,rounded corners,size=small,skin=bicolor,breakable,
	colframe=FrameCol,colback=white,separator sign none,fonttitle=\bfseries,coltitle=black,
	clip upper,detach title,description delimiters none,description color=BaseColor,
	top=6mm,pad at break=1mm,frame hidden,arc=3mm,before
	skip=15pt,after skip=15pt,
	overlay ={\node[below right, xshift=-2.4pt, yshift=4.2pt] at (interior.north west) {\tcbtitle};
		\draw[thick,FrameCol,rounded corners=1ex] (interior.north west)--(interior.south west)--(interior.south east);\node[FrameCol, xshift=-5pt, yshift=10pt] at (interior.south east) {$\blacksquare$};},
	overlay first ={\node[below right, xshift=-2.4pt, yshift=4.2pt] at (interior.north west) {\tcbtitle};
		\draw[thick,FrameCol,rounded corners=1ex] (interior.north west)--(interior.south west);},
	overlay middle={\draw[thick,FrameCol,rounded corners=1ex] (interior.north west)--(interior.south west);},
	overlay last={\node[FrameCol, xshift=-5pt, yshift=10pt] at (interior.south east) {$\blacksquare$};
		\draw[thick,FrameCol,rounded corners=1ex] (interior.north west)--(interior.south west)--(interior.south east);},
}{proof}{PropBoxTitle}

% ---------------------------------------- Simple highlighting box styling -----------------------------------------
\newtcolorbox{highlight}{
	enhanced,
	fonttitle=\sffamily\bfseries,
	boxrule=0pt,frame hidden,breakable,
	borderline west={3pt}{0pt}{BaseColor!50!white},
	colback=BaseColor!10!white,left=8mm,right=2mm,before skip=15pt,after skip=15pt,
}

% ---------------------------------------- Example box styling ----------------------------------------------------
\makeatletter
\renewcommand{\new@tcbtheorem}[5][]{%
	\@@newtcolorbox[auto counter,#1]{#2}[3][]{#4,%
		title={\tcb@theo@title{\emph{#3 \thetcbcounter}}{\space}{\textit{\textcolor{black}{{\fontseries{m}\selectfont ##2}}}}},%
		list entry={\protect\numberline{\thetcbcounter}##2},%
		nameref={##2},%
		code={\tcb@theo@label{#5}{##3}},%
		##1}}
\makeatother	

\newtcbtheorem[number within=section]{example}{Example}{%
	enhanced jigsaw,rounded corners,size=small,skin=bicolor,breakable,
	colframe=FrameCol,colback=white,separator sign none,fonttitle=\bfseries,coltitle=black,
	clip upper,detach title,description delimiters none,description color=BaseColor,before
	skip=7pt,after skip=7pt,
	top=5mm,pad at break=1mm,frame hidden,arc=3mm,bottom=1mm,
	overlay ={\node[below right, xshift=-2.4pt, yshift=4.2pt] at (interior.north west) {\tcbtitle};
		      \node[FrameCol, xshift=-5pt, yshift=10pt] at (interior.south east) {$\blacklozenge$};},
	overlay first ={\node[below right, xshift=-2.4pt, yshift=4.2pt] at (interior.north west) {\tcbtitle};},
	overlay middle={},
	overlay last={\node[FrameCol, xshift=-5pt, yshift=10pt] at (interior.south east) {$\blacklozenge$};},}{ex}

\tikzstyle{fillstyle}=[rectangle,fill=white]
\newtcolorbox[number within=section]{ExampleShort}{%
	enhanced,
	colframe=gray,
	colback=white,
	breakable=true,
	fonttitle=\bfseries, 
	colbacktitle=white,
	coltitle=black,
	left=1.8mm,
	right=1.8mm,
	top=1.7mm,
	bottom=1.2mm,
	overlay ={\node[fillstyle,below right, xshift=0.5em, yshift=0.85em] at (interior.north west) {\emph{\textbf{Example}}};},overlay middle={},overlay last={},}


	
% ---------------------------------------- Remark styling ----------------------------------------------------
\newtcbtheorem[number within=section]{remark}{Remark}{%
	enhanced jigsaw,rounded corners,size=small,skin=bicolor,breakable,
	colframe=FrameCol,colback=white,separator sign none,fonttitle=\bfseries,coltitle=black,
	clip upper,detach title,description delimiters none,description color=BaseColor,
	top=5mm,pad at break=1mm,frame hidden,arc=3mm,before
	skip=7pt,after skip=7pt,
	overlay ={\node[below right, xshift=-2.4pt, yshift=4.2pt] at (interior.north west) {\tcbtitle};
		\node[FrameCol, xshift=-5pt, yshift=10pt] at (interior.south east) {$\blacktriangle$};},
	overlay first ={\node[below right, xshift=-2.4pt, yshift=4.2pt] at (interior.north west) {\tcbtitle};},
	overlay middle={},
	overlay last={\node[FrameCol, xshift=-5pt, yshift=10pt] at (interior.south east) {$\blacktriangle$};},}{rq}


% ---------------------------------------- Note styling ----------------------------------------------------
\makeatletter
\renewcommand{\new@tcbtheorem}[5][]{%
	\@@newtcolorbox[auto counter,#1]{#2}[3][]{#4,%
		title={\tcb@theo@title{\emph{#3 }}{\space}{$ $}},%
		list entry={\protect\numberline{\thetcbcounter}##2},%
		nameref={##2},attach title to upper,
		code={\tcb@theo@label{#5}{##3}},%
		##1}}
\makeatother	
\newtcbtheorem[number within=section]{note}{Note}{%
	fontupper=\itshape,
	enhanced jigsaw,sharp corners,size=minimal,skin=bicolor,breakable,before
	skip=7pt,after skip=7pt, colframe=FrameCol,colback=white,separator sign none,fonttitle=\bfseries,coltitle=black,
	clip upper,detach title,description delimiters none,description color=BaseColor,
	top=3mm,pad at break=1mm,frame hidden,left=0mm,bottom=3mm,
	overlay ={\node[FrameCol, xshift=-5pt, yshift=10pt] at (interior.south east) {$\blacktriangle$};},
	overlay first ={},
	overlay middle={},
	overlay last={\node[FrameCol, xshift=-5pt, yshift=6pt] at (interior.south east) {$\blacktriangle$};},}{nt}


% =====================================================================================
%						Theming of the section headers
% =====================================================================================
\titleformat{\chapter}[block]
{\normalfont\color{BaseColor}}
{\tcbset{colframe=BaseColor, boxrule=0.8pt, left=0pt, right=0pt, top=0pt, bottom=0pt}\fontsize{24}{30}\selectfont\parbox{0.82\textwidth}{#1}\hfill\mbox{\tcbox[boxsep=12pt, tcbox raise = 0pt]{\color{BaseColor}\bfseries\fontsize{30}{10}\selectfont\thechapter}}}
{0.5em}{\vskip2ex\endgraf\titlerule[0.5ex]}[]

\titleformat{name=\chapter,numberless}[block]{\normalfont\color{BaseColor}}{\tcbset{colframe=BaseColor, boxrule=0.8pt, left=0pt, right=0pt, top=0pt, bottom=0pt}\fontsize{30}{35}\selectfont\parbox{0.7\textwidth}{#1}}{1em}{\vskip1ex\endgraf\titlerule[0.5ex]}[]

\makeatletter
\renewcommand{\section}{
	\@startsection{section}{1}{0pt}
	{-3.5ex plus -1ex minus -0.2ex}
	{2.3ex plus 0.2ex}
	{\color{TextColor}\normalfont\Large\bfseries}}

\renewcommand\subsection{
	\@startsection{subsection}{2}{\z@}
	{-3.25ex\@plus -1ex \@minus -.2ex}
	{1.5ex \@plus .2ex}
	{\color{TextColor}\normalfont\large\bfseries}}

\renewcommand\subsubsection{
	\@startsection{subsubsection}{3}{\z@}
	{-3.25ex\@plus -1ex \@minus -.2ex}
	{1.5ex \@plus .2ex}
	{\color{TextColor}\normalfont\fontsize{14}{16}\bfseries}}

\renewcommand\paragraph{
	\@startsection{paragraph}{4}{\z@}
	{-3.25ex\@plus -1ex \@minus -.2ex}
	{-1em}
	{\color{TextColor}\normalfont\normalsize\bfseries}}

\renewcommand\subparagraph{
	\@startsection{subparagraph}{5}{\z@}
	{-3.25ex\@plus -1ex \@minus -.2ex}
	{-1em}
	{\color{TextColor}\normalfont\normalsize\bfseries}}
\makeatother


% =====================================================================================
%					Theming of the pages headers
% =====================================================================================
% Getting the offset of the separators 
\headsep=1.8em

% Definition of classical pages layout
\pagestyle{fancy}	% Load default style
\renewcommand{\chaptermark}[1]{ \markboth{\thechapter. \space #1}{} }
\renewcommand{\sectionmark}[1]{ \markright{\thesection. \space #1}{} }
\fancyhf{}
\fancyhead[CE]{ \fontencoding{T1}\fontfamily{pnc}\fontsize{10}{10}\selectfont \textsc{\leftmark}}
\fancyhead[CO]{\fontencoding{T1} \fontfamily{pnc}\fontsize{10}{10}\selectfont \textsc{\rightmark}}
\fancyfoot[RO]{ \fontencoding{T1}\fontfamily{pnc}\fontsize{10}{0}\selectfont \thepage}
\fancyfoot[LE]{ \fontencoding{T1}\fontfamily{pnc}\fontsize{10}{0}\selectfont \thepage}


% Definition of endchapter pages style
\fancypagestyle{endchapter}{
	\fancyhead[CE]{\fontencoding{T1}\fontfamily{pnc}\fontsize{10}{10}\selectfont\textsc{\textsc{\leftmark}}}
	\fancyhead[CO]{\fontencoding{T1}\fontfamily{pnc}\fontsize{10}{10}\selectfont\textsc{\rightmark}}
	\fancyfoot[RO]{\fontencoding{T1}\fontfamily{pnc}\fontsize{10}{0}\selectfont}
	\fancyfoot[LE]{ \fontencoding{T1}\fontfamily{pnc}\fontsize{10}{0}\selectfont}}

% =====================================================================================
%				 Theming abstract/thanks pages
% =====================================================================================
% Changing the abstract title color
\makeatletter
\renewenvironment{abstract}{%
	\vspace*{\fill}
	\begin{center}%
		{\bfseries \textcolor{BaseColor}{\abstractname}\vspace{\z@}}
	\end{center}%
	\thispagestyle{empty}

}
{\vspace*{\fill}\if@twocolumn\else\endquotation\fi}
\makeatother

% Changing the aknoledgment title color
\makeatletter
\renewenvironment{thanks}{%
	\vspace*{\fill}
	\begin{center}%
		{\bfseries \textcolor{BaseColor}{Acknowledgments}\vspace{\z@}}
	\end{center}%
	\thispagestyle{empty}
}
{\vspace*{\fill}\if@twocolumn\else\endquotation\fi}
\makeatother

% =====================================================================================
%				 Theming of the bibliography and table of contents
% =====================================================================================
% Say that the toc is only the toc
\makeatletter
\renewcommand\tableofcontents{\@starttoc{toc}}
\makeatother

% Theming the table of contents and section numbering
\setcounter{secnumdepth}{3}			% number subsubsections
\setcounter{tocdepth}{2}			% and include them in the TOC
\setcounter{minitocdepth}{2}
\renewcommand{\mtctitle}{}
\mtcsetrules{*}{off}

\newcommand{\smalltoc}{
	\begin{tcolorbox}[
			width=\linewidth,
			enhanced,
			top=10pt,
			left=-12pt,
			nobeforeafter,
			outer arc=15pt,
			arc=15pt,
			boxrule=0.8pt,
			colback=white,
			colframe=BaseColor,
			overlay={
				\node[anchor=west,fill=white,inner xsep=6pt] 
				at ([xshift=15pt]frame.north west) 
				{\Large \textbf{\textcolor{BaseColor}{Chapter contents}}};
			}
			]
			\minitoc
	\end{tcolorbox}
}


\newcommand{\begintoc}{
	\begin{tcolorbox}[
		width=\linewidth,
		enhanced,
		top=10pt,
		left=8pt,
		bottom=8pt,
		nobeforeafter,
		outer arc=15pt,
		arc=15pt,
		boxrule=0.8pt,
		colback=white,
		colframe=BaseColor,
		overlay={
			\node[anchor=west,fill=white,inner xsep=6pt]
			at ([xshift=15pt]frame.north west) 
			{\Large \textbf{\textcolor{BaseColor}{Thesis overview}}};
		}
		]
		\shorttableofcontents
	\end{tcolorbox}
}

% Say that the toc is only the toc
\makeatletter
\newcommand\onlytableofcontents{\@starttoc{toc}}
\makeatother

\makeatletter
\renewcommand\shorttableofcontents{\bgroup\c@tocdepth=1\@startshorttoc{toc}\egroup}
\makeatother

% Custom table of contents and page shaping
\newcommand{\tocName}{\textcolor{BaseColor}{Table of Contents}}
\addto\captionsenglish{\renewcommand{\contentsname}{\tocName}}

% Custom list of figures  and page shaping
\newcommand{\lofName}{\textcolor{BaseColor}{List of figures}}
\addto\captionsenglish{\renewcommand{\listfigurename}{\lofName}}

% Custom list of tables and page shaping
\newcommand{\lotName}{\textcolor{BaseColor}{List of tables}}
\addto\captionsenglish{\renewcommand{\listtablename}{\lotName}}

% Separate Lof/Lot by chapter
\makeatletter
\newcounter{chapter@last@figure}
\newcounter{chapter@last@table}
\pretocmd{\caption}{
	\ifnum\pdfstrcmp{\@captype}{figure}=0
	\ifnum\value{chapter}=\value{chapter@last@figure}\else
	\addtocontents{lof}
	{\protect\numberline{\bfseries\thechapter\quad\thechaptername}}%
	\fi
	\fi
	\ifnum\pdfstrcmp{\@captype}{table}=0
	\ifnum\value{chapter}=\value{chapter@last@table}\else
	\addtocontents{lot}
	{\protect\numberline{\bfseries\thechapter\quad\thechaptername}}%
	\fi
	\fi  
	\expandafter\setcounter\expandafter{chapter@last@\@captype}{\value{chapter}}%
}{}{}
\makeatother

% Bibliography style and pointer
\DeclareLanguageMapping{english}{abbrv} %Literaturverzeichnis american-apa style
