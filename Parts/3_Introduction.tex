\chapter{Introduction} \label{chap:Introduction}

The approximation of solutions to mathematical problems plays a significant role in the visualisation of their features while providing insights to the very nature of the problem itself. In turn, a numerical method that has been designed for taking into account specific features of a given dynamics can then be applied to a large range of other problems where similar behaviours are encountered, thereby providing a general approximation framework. A typical class of problems whose formulation and solutions share common features is the class of problems modelling conservative physical behaviours, the involved principles yielding dynamical systems that have a comparable structure, as pointed out by Poincaré back in 1890 \cite{poincare1890equations}.

\section{Problem of interest and general difficulties}
The specific structure that we are interested in in this thesis is the class of time-dependent problems governed by a set of conservative equations, whose system is of hyperbolic nature and dynamics preserves the total quantity of each variable in time. Those problems usually deriving directly from physical principles, their numerical approximation find natural applications in a wide spectrum of areas, ranging from engineering and environmental modelling, as \emph{e.g.} for alloy development \cite{ross1988ion}, weather prediction \cite{kasahara1977computational}, to finding the origin of a river pollution \cite{alvarez2009application} or even to determine a best wild-fire-fighting strategy \cite{wiitala1999dynamic}. Those applications requiring accurate and reliable approximate solutions to the considered problems, there is a strong demand for accurate solvers. However, the intrinsic nature of the problems makes their development tremendously complicated.

Indeed, any approximation relies on a discretisation of the problem, where the solution is made available only at specific points in the time-space. The dynamic has therefore to be represented accurately from a finite set of \emph{so-called} \Dofs values while satisfying a discrete analogue of the involved physical principles and guaranteeing the relevance of the solution. In particular, the designed numerical scheme should ensure that the conservation principle holds at the discrete level for each conserved variable. Otherwise, the quality of the solution would deteriorate over time, and any convergence to the solution of the continuous problem would ultimately be lost. For the solution to be reliable in the long run, the scheme must also be stable, that is, robust to perturbations that may result form the initial data or from computational errors. However, ensuring stability in the context of hyperbolic equations is extremely delicate as discontinuities may appear in the solution, even for smooth initial conditions.
\bigskip 

The treatment of these discontinuities is equally of major importance. Indeed, as they are moving at a finite speed in the spatial domain, their location impact significantly the dynamics, making their accurate determination crucial for the numerical solution to be representative of the comprehensive dynamics behaviour. However, defining an accurate representation is very delicate as those discontinuities are possibly of different natures, being either preserved or weakened in time. Furthermore, as a consequence of the presence of discontinuities, the global regularity of the sought solution has to be weakened, leading to a lack of uniqueness. Hence, at the discrete level the numerical scheme must ensure that the approximate solutions converge to a physically meaningful one. We therefore consider here an entropy-based selection criterion by asking the numerical scheme to be entropy stable.
\bigskip

Lastly, it should also be mentioned that in practical applications, the study domain can have a complex geometry. It is therefore in the interest of numerical schemes to be geometrically flexible, so as to  approximate the solution with the same accuracy all over the computational domain. Moreover, with the recent growth in the size and complexity of the investigated problems, there is a strong demand to converge quickly to the solution, \emph{i.e.} to obtain a reliable solution without much spatial discretisation effort.
\bigskip

\noindent However, the current numerical methods that are dedicated to the approximation of solutions to conservative hyperbolic problems either severely lack precision in determining the physically relevant spatial location of the discontinuities, or fail to ensure the conservation and stability principles, both features being crucial to render a physically relevant approximated solution.
\newpage

\section{Scope of the presented work}

In this thesis, we focus on designing high order schemes that provide a local conservation principle to the retrieved solution while ensuring its homogeneous accuracy across the spatial domain and its physical relevance.
\vspace{-0.1em}

\paragraph{Background choices}
To achieve conservation down to the discrete level, we focus on developing gridded numerical methods. Indeed, there, the computational domain is covered with a finite number of cells, small tiles defining control volumes on which the solution's variables can be quantified, preserved, and where a local conservation can be directly enforced. Furthermore, as we aim the developed schemes to be robust with respect to the computational domain geometry and to be flexible with respect to its discretisation, we consider the particular subclass of hybrid unstructured meshes where the cells can be of various shapes and their layout organised in an irregular pattern.

In this regard, the use of non-convex polygonal meshes appears to particularly relevant, as they allow more flexibility in the geometry description, more robustness towards distortions, and facilitate the development of geometry dependent techniques such as mesh adaptation techniques by automatically including hanging nodes and facilitating local mesh coarsening.
\bigskip

Although this class of schemes has been widely investigated in the context of elliptic problems \cite{zbMATH06666913,VEIGA2013,zbMATH06966728,zbMATH06823756,zbMATH06596741}, their use in the context of hyperbolic systems is relatively recent \cite{Talischi2013,zbMATH07078646,zbMATH05897522}. Indeed, as for any unstructured mesh, there is a trade off between the order of the there constructed scheme, indicating by how much the accuracy of the discrete solution improves when the mesh size is decreased, and the ability of retrieving its possible discontinuities while guaranteeing stability of the approximation \cite{godunov1959difference}. In particular, the presence of discontinuities yields high order schemes to require specific treatments as filtering or limiting techniques to ensure stability \cite{harten1987uniformly,harten1983upstream,jiang1996efficient}, and even to ensure that the obtained discrete solution has a physical meaning \cite{chen2017entropy,huynh2007flux}. While those treatments have been widely investigated in simplicial and quads meshes, the range of existing techniques for cells having a higher number of faces is still limited, their design being much more involved both geometrically and variationally.
\vspace{-0.1em}

\paragraph{Considered scope of this thesis} We therefore aim here at improving the quality and robustness of the approximated solution to conservative hyperbolic problems by developing schemes that are both stable and entropy stable even across discontinuities, without sacrificing the conservation. Those discontinuities arising either in the field of a single variable or in the interaction of physical variables, there creating a \emph{so-called} interface, our developments have taken place on two distinct levels.

\section{Thesis contribution}
Considering first the case where a discontinuity arises in the field of a single variable, we focused on the scheme stability and physical relevance of the retrieved solution, developing a \FR scheme that can be used on any arbitrary hybrid unstructured mesh whose cells are not necessarily convex and an associated theoretical framework to ascertain its well-definition.

We then focused on discontinuities arising in the interaction of physical variables by improving a correction procedure for mass-conservative in two-phase flows. More precisely, we were able to locate continuously the mixture's interface in two-phase flows while preserving the local mass quantities.

\paragraph{Development of an entropy stable \FR scheme} In the first part of the project, we achieved entropy stability by recasting a Flux Reconstruction scheme into the Residual Distribution framework. To this aim, we there used the conservation and stability results of the Residual Distributions schemes to develop an entropy stable \FR scheme in its \RD formulation, before deriving a determination system on the correction function allowing to convert back the expression of the obtained entropy stable scheme in its original \FR framework.

In order to ascertain the existence of such a correction function, we were led to develop a theoretical framework within which there exists at least one function solving the previously developed determination system. Namely, we constructed a class of $H(\div)$--conforming elements that can be set up on any non-convex polytopal cell and that benefit from the properties of the \RT elements on the boundary, using the Virtual Elements framework as a core. We thence obtained a very flexible class of elements that can be used in any dimension, on any possibly non-convex polytope, and whose variational setting can be easily adapted to a large variety of specific needs.

\paragraph{Improvement of an interface capturing scheme} In the second part of the project, we restricted ourselves to the two-dimensional case and considered incompressible two-phase flow problems. We particularly focused on retrieving the time-space dependent location of the mixture's interface so that both the conservation of the partial masses and the global continuity of the interface are obtained. To this aim, we focused on the class of \LS based schemes and developed a continuous and conservative interface correction procedure by improving the already conservative \emph{THINC-LS} technique \cite{kumar2019thinc}.

The \emph{THINC-LS} being based on the characterisation of a corrective constant, we changed its definition to a space-dependent correction function that preserves or enforces the continuity of the corrected interface location. Paying a particular attention to geometrical issues, we could design a correction function that can be set up on any non-convex polygonal cell and whose support stays local to the control volume the interface location is corrected on. Hence, the improved correction procedure now allows to correct the initial interface location that has been achieved through any gridded scheme, enforcing the conservation of the local masses and preserving the global continuity of the interface. In addition, in case of an initially discontinuous interface, the global continuity can also be enforced up to a straightforward extension of the procedure.

\paragraph{Related publications}

\begin{itemize}
	\setlength{\itemsep}{0.1em}
	\item \textit{On the connection between residual distribution schemes and flux reconstruction.} Rémi Abgrall, Élise Le Mélédo, Philipp Öffner.\\
	\textit{Arxiv preprint}, 2018, \url{https://arxiv.org/abs/1807.01261}.\\
	(\SecRef{sec:ConservFR} of \ChapRef{chap:Preliminaries})
	
	\item \textit{A class of finite dimensional spaces and $H(\mathrm{div})$-conformal elements on general polytopes.} Rémi Abgrall, Élise Le Mélédo, Philipp Öffner.\\
	\textit{Technical report}, 2019, \url{https://arxiv.org/abs/1907.08678}.\\
	(\TwoSecRef{sec:ConformalElements}{sec:PolytopeFramework} of \ChapRef{chap:HK})
	
	\item \textit{General polytopal $H(\mathrm{div})$-conformal finite elements and their discretisation spaces.}
	Rémi Abgrall, Élise Le Mélédo, Philipp Öffner.\\
	\textit{ESIAM: M2An}, 2021, \url{https://doi.org/10.1051/m2an/2020048}.\\
	(\SecRef{sec:PolytopeFramework} of \ChapRef{chap:HK})
	
	\item \textit{A corrective conservative and continuity preserving Level Set approach for two phase flows.}
	      Rémi Abgrall, Élise Le Mélédo.\\
	      \textit{In preparation}\\		
		  (\SecRef{sec:CCTHINC} of \ChapRef{chap:LS})
\end{itemize}

\paragraph{Related public implementation}

\begin{itemize}
	\setlength{\itemsep}{0.1em}
	\item \textit{LESARD: LEvel Set Algorithm for Residual Distribution}, open source and user-friendly code solving two-dimensional partial differential equation problems. Élise Le Mélédo\\
	Repository:    \url{https://git.math.uzh.ch/elemel/lesard}\\
	Documentation: \url{https://www.math.uzh.ch/pages/lesard/}
\end{itemize}
\newpage

\section{Outline}

All in all, we focus on developing stable, entropy stable, conservative, high order and geometrically flexible schemes for conservative hyperbolic systems. We start in \ChapRef{chap:Preliminaries} by introducing the principal features of hyperbolic problems before detailing a construction of an entropy stable \FR scheme. We then present in \ChapRef{chap:HK} the theoretical foundations required by the previously presented numerical scheme, and end in  \ChapRef{chap:LS} by detailing the improvement of the \emph{THINC-LS} correction procedure ensuring the space-time dependent resolution of a continuous and conservative interface location in two-phase flows.
\vspace{3em}

\noindent \begintoc

